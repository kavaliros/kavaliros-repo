#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Rebuild all packages v1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

banner "Rebuild all packages"
for D in ./*; do
    if [ -d "$D" ]; then
        cd "$D"
        if [ -f build.sh ]; then
            bash build.sh
        fi
        cd ..
    fi
done
banner "Ready..."

