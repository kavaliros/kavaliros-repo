```
  __  __                     __ __        _______ _______ 
 |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
 |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
 |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
```

Arch Linux custom repository for KavalirOS

## Packages
+ Installers
    + **calamares** - Calamares installer.
    + **pamac** - A Gtk3 frontend for libalpm.
    + **yay** - Yet another yogurt. Pacman wrapper and AUR helper written in go.
+ Theming
    + **bibata-cursor-theme** - Material Based Cursor Theme.
    + **otf-aurulent-sans** - Aurulent Sans is designed to be used as an interface font on free graphics servers such as X.org.
    + **prof-xfce-theme** - GTK3/XFWM4-theme based on Prof-Gnome-theme.
    + **ttf-monaco** - The Monaco monospaced sans-serif typeface with special characters added
    + **zafiro-icon-theme** - A icon pack flat with light colors.
+ Applications
    + **azure-data-studio** - Data management tool that enables you to work with SQL Server, Azure SQL DB and SQL DW 
    + **bitwarden** - A secure and free password manager for all of your devices.
    + **exaile** - A full-featured Amarok-style media player for GTK+
    + **icaclient** - Citrix Workspace App for x86_64 (64bit) Linux (ICAClient, Citrix Receiver)
    + **iriun** - Use your phone's camera as a wireless webcam in your PC.
    + **netextender** - SonicWALL SSL VPN Client
    + **nordvpn** - NordVPN CLI tool for Linux.
    + **parsehub** - Scrape the internet.
    + **postman** - Build, test, and document your APIs faster.
    + **teams** - Microsoft Teams for Linux is your chat-centered workspace in Office 365
    + **virtualbox-ext-oracle** - VirtualBox extensions from Oracle.
    + **visual-studio-code** - Visual Studio Code (vscode): Editor for building and debugging modern web and cloud applications.
    + **zwaarmetaal** - Libary Manager voor Zware Metalen
+ Hardware/drivers
    + **brother-mfc-j4610dw** - Brother printer driver.
    + **brscan4** - Brother scanner driver.
    + **displaylink** - Linux driver for DL-6xxx, DL-5xxx, DL-41xx and DL-3x00.
    + **evdi** - A Linux® kernel module that enables management of multiple screens.

## Deploy repository

The deploy script uses secret-tool the get the password from the keyring

```
secret-tool store --label="FTP" ftp-site username
```
