#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Build package v1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

banner "Remove package" 
set -- zwaarmetaal*.tar.zst
if [ -f "$1" ]; then
    rm -v "$1"
fi
set -- *.gz
if [ -f "$1" ]; then
    rm -v "$1"
fi
if [ -d ./pkg ]; then
    rm -Rf ./pkg
fi
if [ -d src ]; then
    rm -Rf src
fi
banner "Extract archive"
mkdir src
cd src
tar xvf /data/Public/KavalirOS/zwaarmetaal.tar.gz
cd ..
banner "Make package"
makepkg -s
banner "Remove sources" 
set -- *.gz
if [ -f "$1" ]; then
    rm -v "$1"
fi
if [ -d ./pkg ]; then
    rm -Rf ./pkg
fi
if [ -d src ]; then
    rm -Rf src
fi
set -- zwaarmetaal*.tar.zst
banner "Ready building package $1" 
