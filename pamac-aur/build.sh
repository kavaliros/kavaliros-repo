#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Build package v1.1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

function removesources() {
    banner "Removing sources"
    set -- *.gz
    if [ -f "$1" ]; then
        rm  -v "$1"
    fi
    if [ -d ./pkg ]; then
        rm -Rf ./pkg
    fi
    if [ -d src ]; then
        rm -Rf src
    fi
}

banner "Remove package" 
set -- pamac*.tar.zst
if [ -f "$1" ]; then
    rm -v "$1"
fi
removesources

banner "Make package"
makepkg -s

removesources
set -- pamac*.tar.zst
banner "Ready building package $1" 
