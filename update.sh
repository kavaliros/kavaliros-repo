#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Update package v1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}
workfolder=~/Workspace/$1

banner "Clone AUR git"
if [ -d $workfolder ]; then
   rm -Rf $workfolder
fi

git clone https://aur.archlinux.org/$1.git $workfolder

if [ -d $workfolder/.git ]; then
	rm -Rf $workfolder/.git
fi

if [ -f $workfolder/.gitignore ]; then
	rm ~/Workspace/$1/.gitignore
fi

banner "Compare and merge"
meld $workfolder ./$1
banner "Build package"
cd $1
sh ./build.sh
cd ..
