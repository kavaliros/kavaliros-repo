#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Publish all packages v1.0
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols)
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

get_password() {
    secret-tool lookup "$URL" "$LOGIN"
}

banner "Rebuild KavalirOS repository"

REPODIR=./Workspace/Repository

if [[ ! -e $REPODIR ]]; then
    mkdir -p $REPODIR
fi

# Remove current repository
rm $REPODIR/*

# Gather all packages from aur folder
find . -name '*.tar.zst' -not -path "$REPODIR/*"  -exec cp -v '{}' $REPODIR/ \;

# Add create new repository
repo-add $REPODIR/kavaliros.db.tar.gz $REPODIR/*.tar.zst

# Remove symlink
rm $REPODIR/kavaliros.db
rm $REPODIR/kavaliros.files

# Move compressed files to repo files
mv $REPODIR/kavaliros.db.tar.gz $REPODIR/kavaliros.db
mv $REPODIR/kavaliros.files.tar.gz $REPODIR/kavaliros.files

banner "Deploy KavalirOS repository"

LOGIN=riddermanr-001
URL=ftp://win6028.site4now.net
PASSWORD=$( get_password )

lftp -u $LOGIN,$PASSWORD $URL<<EOF
set ftp:ssl-force true
set ftp:ssl-protect-data true
set ssl:verify-certificate false
cd /kavaliros/repo/x86_64
mput ./Workspace/Repository/*
cd ..
exit
EOF

banner "KavalirOS repository published"
