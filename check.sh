#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Check packages v1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

banner "Check KavalirOS repository"

find . -name PKGBUILD -not -path "./Workspace/*" | wc -l
find . -name *.zst -not -path "./Workspace/*" | wc -l

banner "Check versions against AUR"
repotools CheckRepo